var acc = document.getElementsByClassName("accordion");
var i;

for (i = 0; i < acc.length; i++) {
  acc[i].addEventListener("click", function() {

    this.classList.toggle("active");
    var panel = this.nextElementSibling;

    if (panel.style.maxHeight) {
      // decreasing height:
      var diff = - panel.style.maxHeight;
      panel.style.maxHeight = null;
      modHeight(panel.parentElement, diff);

    } else {
      //increasing height:
      panel.style.maxHeight = panel.scrollHeight + "px";
      modHeight(panel.parentElement, panel.scrollHeight);
    }

  });
}

function modHeight(panel, hchange){
    if (panel.className == "panel"){
        panel.style.maxHeight = panel.scrollHeight + hchange + "px";
    }
    if (panel.className == "panel" | panel.nodeName == "DL" ){
        modHeight(panel.parentElement, hchange)
    }
}
