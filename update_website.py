#! /usr/bin/env python3

import os
import subprocess
import contextlib
import glob
import json
import argparse
import pathlib
import tempfile
import requests
import base64
import shutil
import yaml
import re
import textract
import io
import mimetypes
import time

import pickle
# import cloudpickle as pickle

from urllib.parse import urlparse
from numpy.random import choice
from rake_nltk import Rake
from fnmatch import fnmatch
from shlex import split as shsplit
from mozlz4 import decompress as lz4_decomp
from csv2md import csv_to_table, md_table

try:
    from BeautifulSoup import BeautifulSoup

except ImportError:
    from bs4 import BeautifulSoup, Tag

# check availability of cli dependencies
assert not subprocess.check_call(shsplit('bash -c \'pandoc --version >/dev/null 2>&1\'')), print(
    'pandoc is not installed')

webroot = pathlib.Path('./website-personal')
LINK_CLOUD_DATA_FILE = webroot/'link-cloud.pickle'
page_path = webroot/"content"/"pages"


class MozHtmlBMtree(object):

    def __init__(self, soup):
        self.soup = soup

    @staticmethod
    def ishtmldir(elem):
        if elem.name == 'dt' and elem.contents[0].name == 'h3':
            return True
        else:
            return False

    @staticmethod
    def ishtmla(elem):
        if elem.name == 'dt' and elem.contents[0].name == 'a':
            return True
        else:
            return False

    def asdict(self):
        # find root:
        for t in self.soup.descendants:
            if self.ishtmldir(t):
                break

        children = []
        for t in t.find_next_siblings('dt'):
            children.append(MozHtmlBMcontainter(t).asdict)

        dd = {
            'title': 'root',
            'type': MozJSON.dir_tag,
            'children': children,
        }

        return dd


class MozHtmlBMcontainter(object):

    def __init__(self, tag):
        self.tag = tag

    @property
    def links(self):
        return [MozHtmlBMentry(l) for l in self.tag.contents[2].contents if MozHtmlBMtree.ishtmla(l)]

    @property
    def subdirs(self):
        return [MozHtmlBMcontainter(s) for s in self.tag.contents[2].contents if MozHtmlBMtree.ishtmldir(s)]

    @property
    def asdict(self):

        dd = {
            'title': self.tag.contents[0].string,
            'type': MozJSON.dir_tag,
            'children': [],
        }

        for s in self.subdirs:
            dd['children'].append(s.asdict)

        for l in self.links:
            dd['children'].append(l.asdict)

        return dd


class MozHtmlBMentry(object):

    def __init__(self, tag):
        self.tag = tag

    @property
    def asdict(self):
        return {
            'title': self.tag.contents[0].string,
            'uri': self.tag.contents[0]['href'],
            'type': MozJSON.link_tag,
            'visits': self.tag.contents[0]['href']
        }


class MozJSON(object):
    dir_tag = 'text/x-moz-place-container'
    link_tag = 'text/x-moz-place'

    def __init__(self, loadspec=None, jd=None):

        if jd is not None:
            self.jd = jd

        if loadspec is not None:
            self.jd = self.load_data(loadspec)

    def filter(self, ffc, fflc):
        needs = ffc['folders']
        rejects = [s for c in fflc['collections'] if c != ffc for s in c['folders'] if '*' not in s]
        keeps = MozJSON(jd={
            'title': fflc['master'],
            'type': MozJSON.dir_tag,
            'children': []
        })

        for j in self.walk(maxdepth=1):
            if MozJSON.isdir(j) and any([fnmatch(j['title'].lower(), c.lower()) for c in needs]) and not any(
                [fnmatch(j['title'].lower(), r.lower()) for r in rejects]):
                keeps.jd['children'].append(dict(j))

        for j in keeps.walk(maxdepth=1):
            j['title'] = j['title'].lower().replace('public', '').strip('-:').title()

        return keeps

    def walk(self, **kwargs):
        yield from self.dowalk(self.jd, **kwargs)

    @staticmethod
    def isdir(node):
        return True if node['type'] == MozJSON.dir_tag else False

    @staticmethod
    def islink(node):
        return True if 'uri' in node else False

    @staticmethod
    def dowalk(json, path=pathlib.PurePath('/'), maxdepth=None):

        yield json

        if MozJSON.isdir(json) and 'children' in json and json['children']:

            if maxdepth is not None:
                cd = len(path.name.split('/')) - 1
                if cd >= maxdepth:
                    return

            for c in json['children']:
                yield from MozJSON.dowalk(c, path=path / json['title'], maxdepth=maxdepth)

    @staticmethod
    def load_data(fflc, mode='backup'):
        """ This finds either the bookmark backups in your '~/.mozilla/firefox/*.default-release/bookmarkbackups/' path
        or the html '~/.mozilla/firefox/*.default-release/bookmarks.html' file
        and automatically cleans them, and creates json files and markdown files in your website.
        You can make Firefox create an HTML backup of the bookmarks each time Firefox is closed by setting the
                browser.bookmarks.autoExportHTML pref to true on the about:config page.
                    http://kb.mozillazine.org/browser.bookmarks.autoExportHTML
                    http://kb.mozillazine.org/Export_bookmarks
                You can set the location of the backup by setting a browser.bookmarks.file pref (not there by default)
                    http://kb.mozillazine.org/browser.bookmarks.file
    """

        ff_root = pathlib.Path(resolve(('~/.mozilla/firefox/*' + fflc['profile'] + '*')))

        if mode is not None and 'html' in mode.lower():
            assert (ff_root / 'bookmarks.html').is_file(), "html mode specified but FF \'bookmarks.html\' file" \
                                                           " not present. Check FF's \'about:config\'"
            with open(ff_root, 'r') as f:
                soup = BeautifulSoup(f.read(), 'html5lib')
            jd = MozJSON(MozHtmlBMtree(soup).asdict())

        elif mode is not None and 'backup' in mode.lower():

            bkdir = ff_root / 'bookmarkbackups'

            assert bkdir.is_dir(), "can't find Firefox \'bookmarkbackups\' directory.  " \
                                   "Make sure bookmark backups are enabled in Firefox."

            bkjson = sorted(os.listdir(bkdir))[-1]

            with open(os.path.join(bkdir, bkjson), 'rb') as f:
                jd = MozJSON(jd=json.loads(lz4_decomp(f).decode()))

            for j in jd.walk():
                if j['title'] == fflc['master']:
                    return j

        else:
            raise NotImplementedError('Bad \'mode\' parameter to MozJSON.load_data()')


class LinkCloudItem(object):

    def __init__(self, title, url, visit_count=None, text=None):
        self.title = title
        self.url = url
        self.visits = visit_count
        self.keywords = None

    def __eq__(self, other):
        if type(other) is not type(self):
            return False
        if self.title == other.title and self.url == other.url:
            return True
        return False


class LinkCloud(list):

    def append(self, item):
        if isinstance(item, MozJSON):
            super().append(LinkCloudItem(url=item['uri'], title=item['title']))
        if isinstance(item, LinkCloudItem):
            super().append(item)
        else:
            raise NotImplementedError


def latex2page(source, targ, **kwargs):
    path, sf = os.path.split(resolve(source))
    with pushd(path):
        md = subprocess.check_output(shsplit('pandoc -f latex -t markdown {}'.format(str(sf)))).decode()

    write_markdown(
        metadata=kwargs,
        content=md,
        file=webroot/targ.lstrip('/'),
    )


def csv2page(source, targ, **kwargs):
    source = resolve(source)

    if source.endswith('.ods'):
        subprocess.check_call(shsplit(
            'soffice --headless --convert-to csv --outdir {} {} '.format(tempfile.gettempdir(), source)
        ))
        source = os.path.join(tempfile.gettempdir(), os.path.split(source)[1].replace('.ods', '.csv'))

    with open(source, 'r') as f:
        table = csv_to_table(f, ',')

    if not table:
        return

    if any([e.startswith('hyperlink') for e in table[0]]) and any([e.startswith('Description') for e in table[0]]):
        colind = table[0].index('hyperlink')
        descind = table[0].index('Description')
        for i, row in enumerate(table):
            if i > 0:
                row[descind] = '[{}]({})'.format(row[descind], row[colind])
            del row[colind]

    write_markdown(
        metadata=kwargs,
        content=md_table(table),
        file=webroot / targ.lstrip('/'),
    )


def ff2cloud(ffc, fflc, jd, linkcloud):
    keeps = jd.filter(ffc, fflc)

    for j in keeps.walk():
        if MozJSON.islink(j):
            linkcloud.append(LinkCloudItem(url=j['uri'], title=j['title']))

    if clargs.yes or check('Do you want to populate linkCloud item text?', default=False):

        lo_linkcloud = None

        if LINK_CLOUD_DATA_FILE.is_file():
            with open(LINK_CLOUD_DATA_FILE, 'rb') as f:
                lo_linkcloud = pickle.load(f)

        lll = len(linkcloud)
        for i, lk in enumerate(linkcloud):
            print('link {}/{} ::::::::::::::::::::::::::::::::::'.format(i, lll))
            if lo_linkcloud and lk in lo_linkcloud:
                idx = lo_linkcloud.index(lk)
                if lo_linkcloud[idx].keywords:
                    print('loading keywords from saved database... ({})'.format(lk.url))
                    lk.keywords = lo_linkcloud[idx].keywords
                    continue

            print('scraping keywords from the web...({})'.format(lk.url))
            text = popLCtext(lk.url)
            if text and isinstance(text, str):
                raker = Rake()
                raker.extract_keywords_from_text(text)
                text = raker.get_ranked_phrases()[:100]
            lk.keywords = text
            with open(LINK_CLOUD_DATA_FILE, 'wb') as f:
                pickle.dump(linkcloud, f)

        corpus = []
        for lk in linkcloud:
            if isinstance(lk.keywords, list):
                corpus.extend(lk.keywords)

        r = Rake()
        r.extract_keywords_from_text('/n'.join(corpus))
        ranks = r.get_ranked_phrases()

def getPdfText(content, tool='pdftotext'):

    tt = ''
    uu = []

    try:
        if tool == 'pyPdf':

            pdf = PyPDF2.PdfFileReader(io.BytesIO(content))

            for page in range(pdf.getNumPages()):
                pt = pdf.getPage(page).extractText()
                tt += '\n' + pt
                # try:
                #     for item in (pdfpage['/Annots']):
                #         a = item.getObject()
                #         uu.append(a['/A']['/URI'])
                # except KeyError:
                #     pass

        elif tool == 'tika':
            tt = tikaparser.from_buffer(io.BytesIO(content))['content']

        elif tool == 'pdfminer':
            tt = pdfminer.high_level.extract_text(io.BytesIO(content))

        elif tool == 'xpdf':
            tt = subprocess.check_output(['pdftotext', '-', '-'], input=content).decode()

        elif tool == 'pdftotext':
            pdf = pdftotext.PDF(io.BytesIO(content))
            tt = 'n'.join(pdf)

        else:
            raise NotImplementedError

    except NotImplementedError as e:
        raise NotImplementedError('unknown pdf parser')

    except:
        return None, None

    uu.extend(striplinks(tt))

    return tt, list(set(uu))


def striplinks(text):
    return [it.rstrip().rstrip(' .,') for it in re.findall('https?://.*', text)]


def popLCtext(url, depth=0, max_depth=1, samedomain=True, max_n_links=25):

    SAMPLE_N = int(max_n_links)

    print('processing url ({})'.format(url))

    guesstype, _ = mimetypes.guess_type(url, strict=False)
    if guesstype and 'image' in guesstype:
        return 0
    if clargs.pdf_only:
        if not guesstype or 'pdf' not in guesstype:
            return None
    try:
        r = requests.get(url, timeout=60)

    except:

        print('exception during http request')
        return -1

    if r.status_code != 200:
        print('http request returned status other than 200')
        return 404

    if ('Content-Type' in r.headers and 'text/html' in r.headers['Content-Type'].lower()) \
        or re.search('<!\s*doctype( \S+)*(?= html) html( \S+)*\s?>', r.text[:250].lower(), flags=re.DOTALL) \
        or re.search('<?\s*xml( \S+)*\s?>', r.text[:250].lower(), flags=re.DOTALL):
        print('processing as html/xml')
        tt = scrapetext(r)
        uu = collect_links(r)

    elif ('Content-Type' in r.headers and '/pdf' in r.headers['Content-Type'].lower()) or url.endswith('.pdf'):
        print('processing as pdf')
        tt, uu = getPdfText(r.content, tool=clargs.pdf_processor)

        if tt == '':

            try:
                tt = textract.process(r.content, method='tesseract', encoding='utf-8')
                uu = striplinks(tt)

            except:
                return None

    else:
        tt = ''

    if depth < max_depth:
        # recurse into discovered links:

        uu = [u if u.startswith('http') else 'https://'+u for u in list(set(uu))]

        if samedomain:
            thisdomain = '.'.join(urlparse(url).netloc.split('.')[-2:]).rstrip('/')
            skips = []
            for u in uu:
                if thisdomain not in urlparse(u).netloc:
                    skips.append(u)
            for su in skips:
                del uu[uu.index(su)]

        if len(uu) > SAMPLE_N:
            uu = choice(uu, SAMPLE_N, replace=False)

        for u in uu:
            st = popLCtext(u, depth=depth + 1, max_depth=max_depth)
            if isinstance(st, str):
                tt += '\n' + st

    return tt


def scrapetext(response):
    bs = BeautifulSoup(response.text, 'lxml')
    text = bs.get_text().encode('ascii', 'ignore').decode().strip()
    while '\n\n' in text:
        text = text.replace('\n\n', '\n')
    text = re.sub('{.*}', '', text)

    return text


def collect_links(response):
    bs = BeautifulSoup(response.text, 'lxml')
    lls = [link.attrs['href'] for link in bs.find_all('a') if
           'href' in link.attrs and link.attrs['href'].startswith('http')]
    return lls


def ff2pages(ffc, fflc, jd):
    keeps = jd.filter(ffc, fflc)
    targgg = ffc['json-path'].lstrip('/')
    os.makedirs(os.path.split(webroot / targgg)[0], exist_ok=True)

    with open(webroot/targgg, 'w') as f:
        json.dump(keeps.jd, f)

    write_markdown(
        metadata={'title': ffc['title'], **ffc['metadata']},
        content='\n\n'.join(
            ['{{{{< accordion \"{}\" \"{}\" >}}}}'.format(ffc['json-path'], match['title'])
             for match in keeps.jd['children']]),
        file=webroot / ffc['page-path'].lstrip('/'),
    )


def git2pages(git_item):
    path = webroot / 'content' / 'project' / git_item['title']
    file = path / 'index.md'

    if not git_item['enabled'] and os.path.isdir(path):
        shutil.rmtree(path)
        return

    if 'gitlab.com' in git_item['url']:

        gurl = Gitlab_URL(git_item['url'])
        response = requests.get(''.join([
            GITLAB_PREFIX,
            'projects/' + gurl.repo + '/repository/files/',
            asurl('README.md'),
            '?ref=master',
        ]))

        if response.status_code == 200:
            mdf = base64.b64decode(json.loads(response.content.decode())['content']).decode()
        else:
            mdf = None

    elif '//github.com' in git_item['url']:
        raise NotImplementedError

    else:
        raise NotImplementedError

    if mdf:
        os.makedirs(path, exist_ok=True)

        write_markdown(
            metadata={'title': git_item['title'], **git_item['metadata']},
            content=mdf,
            file=file,
        )


def git_push():
    if check('do you want to push to the remote repo? (N/y)', default=False):
        with pushd(webroot):
            subprocess.check_call(shsplit('git commit -am "automated update from \'update_website.py\'"'))
            subprocess.check_call(shsplit('git push'))


def process_tex(latex_src):
    for lt in latex_src:
        if clargs.yes or check('Do you want to update "{}" ? (Y/n): '.format(lt['page-path']), default=True):
            latex2page(lt['src-path'], lt['page-path'], **lt['metadata'])


def process_csv(csv_src):
    for cp in csv_src:
        if clargs.yes or check('Do you want to update "{}" ? (Y.n): '.format(cp['page-path']), default=True):
            csv2page(cp['src-path'], cp['page-path'], **cp['metadata'])


def process_ff(fflc):
    jd = MozJSON(loadspec=fflc)
    linkcloud = LinkCloud()

    for ffc in fflc['collections']:
        if clargs.yes or check('Do you want to update "{}" ? (Y.n): '.format(ffc['title']), default=True):
            ff2pages(ffc, fflc, jd)
            ff2cloud(ffc, fflc, jd, linkcloud)


def process_git(git_src):
    for item in git_src:
        git2pages(item)


def iswebroot(path):
    try:
        assert os.path.isdir(os.path.join(path, 'layouts'))
        assert os.path.isdir(os.path.join(path, 'data'))
        assert os.path.isdir(os.path.join(path, 'config'))
        assert os.path.isdir(os.path.join(path, 'content'))
        assert os.path.isdir(os.path.join(path, 'assets'))
        assert os.path.isdir(os.path.join(path, 'themes'))
        assert os.path.isdir(os.path.join(path, 'themes', 'academic'))
        return True

    except AssertionError:
        return False


def check(msg, default=False):
    x = input(msg).lower()
    if (not x and default) or x == 'y':
        return True
    else:
        return False


def frontmatter(**kwargs):
    ym = yaml.dump(kwargs)
    return '---\n' + ym + '\n\n---\n'


@contextlib.contextmanager
def pushd(new_dir):
    previous_dir = os.getcwd()
    os.chdir(new_dir)
    try:
        yield
    finally:
        os.chdir(previous_dir)


def resolve(file):
    return glob.glob(os.path.expanduser(file))[0]


GITLAB_PREFIX = "https://gitlab.com/api/v4/"


def asurl(str):
    return str.lstrip('/').replace(' ', '%20').replace('/', '%2F')


class Gitlab_URL(object):
    def __init__(self, url):
        url = url.split('gitlab.com')[1].lstrip('/')
        self.url = asurl(url)
        self.repo = asurl('/'.join(url.split('/')[:2]))
        self.file = asurl('/'.join(url.split('/')[2:]))


def write_markdown(metadata, content, file):
    with open(file, 'w') as f:
        f.write(frontmatter(**metadata))
        f.write('\n')
        f.write(content)


def main():
    global webroot

    if not iswebroot(webroot):
        webroot = pathlib.Path(str(input("Enter the path to the website root (./website-personal): ") or "website-personal/"))
        assert iswebroot(webroot)

    if not (webroot / 'external_depends.py').is_file():
        raise FileNotFoundError('missing "external_depends.py" in <webroot>({}).'.format(webroot))

    print('Running routine configured in {}/external_depends.py'.format(webroot))
    from external_depends import latex_src, firefox_link_collections, csv_src, git_src

    process_tex(latex_src)
    process_csv(csv_src)
    process_ff(firefox_link_collections)
    process_git(git_src)

    git_push()

parser = argparse.ArgumentParser()
parser.add_argument('--yes', '-y', action='store_true')
parser.add_argument('--pdf-only', '-P', action='store_true')
parser.add_argument('--pdf-processor', '-p', action='store', default='pdftotext',
                    choices=['xpdf', 'tika', 'pdftotext', 'pyPdf', 'pdfminer'])
clargs, unk = parser.parse_known_args()

if clargs.pdf_processor == 'tika':
    from tika import parser as tikaparser
elif clargs.pdf_processor == 'pdftotext':
    import pdftotext
elif clargs.pdf_processor == 'pyPdf':
    import PyPDF2
elif clargs.pdf_processor == 'pdfminer':
    import pdfminer.high_level

if __name__ == '__main__':
    main()
