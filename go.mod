module github.com/wowchemy/starter-academic

go 1.15

require (
	github.com/wowchemy/wowchemy-hugo-modules/netlify-cms-academic v0.0.0-20201028005146-c055788cab2a // indirect
	github.com/wowchemy/wowchemy-hugo-modules/wowchemy v0.0.0-20201028005146-c055788cab2a // indirect
)
