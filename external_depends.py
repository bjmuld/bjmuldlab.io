firefox_link_collections = {
    # 'profile': 'yrlw1kbr',
    # 'master': 'menu',
    # 'collections': [
    #     {'title': 'Web Bookmarks',
    #      'metadata': {'type': 'accordion'},
    #      'json-path': '/content/pages/bookmarks/bookmarks.json',
    #      'folders': ['public:*', '*-public'],
    #      'page-path': '/content/pages/bookmarks/index.md',
    #      },
    #
    #     {'title': 'Important Reads',
    #      'metadata': {'type': 'accordion'},
    #      'json-path': '/content/pages/readings/reading.json',
    #      'page-path': '/content/pages/readings/index.md',
    #      'folders': ['Fundamental Reading-Public'],
    #      },
    # ],
}

latex_src = [

    # {'metadata': {'title': 'Statement of Research', 'draft': True},
    #  'src-path': 'faculty-materials/statement-research/muldrey_research.tex',
    #  'page-path': '/content/pages/research_statement.md',
    #  },
    #
    # {'metadata': {'title': 'Statement of Teaching', 'draft': True},
    #  'src-path': 'faculty-materials/statement-teaching/muldrey_teaching.tex',
    #  'page-path': '/content/pages/teaching_statement.md',
    #  },
    #
    # {'metadata': {'title': 'Statement of Diversity', 'draft': True},
    #  'src-path': 'faculty-materials/statement-diversity/muldrey_diversity.tex',
    #  'page-path': '/content/pages/diversity_statement.md',
    #  },
]

csv_src = [

    # {'metadata': {'title': 'Conferences'},
    #  'src-path': 'targets/conferences.ods',
    #  'page-path': '/content/pages/events_and_dates.md'
    #  },

    # {'metadata': {'title': 'RFPs'},
    #  'src-path': 'targets/RFPs.csv',
    #  'page-path': '/content/pages/RFPs.md'
    #  },

    # {'metadata': {'title': 'Journals'},
    #  'src-path': 'targets/journals.csv',
    #  'page-path': '/content/pages/journals.md'
    #  },
]

git_src = [

    {'title': 'xanity',
     'metadata': {
         'summary': """Xanity is an experimentation benchtop. Experiments are easily parameterized
            and chained into dataflow graphs. Experimental data and meta-data are
            transparently managed. System virtual environments are managed and made portable.""",
     },
     'url': 'https://gitlab.com/bjmuld/xanity',
     'enabled': True,
     },

    {'title': 'songbirt',
     'metadata': {
         'summary': """Songbirt is a lightweight web-based songbook for musicians""",
     },
     'url': 'https://gitlab.com/bjmuld/songbirt',
     'enabled': True,
     },

    {'title': 'dupehawk',
     'metadata': {
         'summary': """Dupehawk is a fast multilevel hashing duplicate-data detector.""",
     },
     'url': 'https://gitlab.com/bjmuld/dupehawk',
     'enabled': True,
     },

    {'title': 'gitall',
     'metadata': {
         'summary': """CLI tool for performing operations over multiple side-by-side git repos.""",
     },
     'url': 'https://gitlab.com/bjmuld/gitall',
     'enabled': True,
     },

    {'title': 'libpsf-python',
     'metadata': {
         'summary': """Python extension for reading Cadence binary psf circuit simulation output.""",
     },
     'url': 'https://gitlab.com/bjmuld/libpsf-python',
     'enabled': True,
     },

    {'title': 'circuitgym',
     'metadata': {
         'summary': """OpenAI Gym -like interface for ML in circuit simulator environments.""",
     },
     'url': 'https://gitlab.com/bjmuld/circuitgym',
     'enabled': True,
     },

    {'title': 'waverunner',
     'metadata': {
         'summary': """Python RPC client/server for ATE automation.""",
     },
     'url': 'https://gitlab.com/bjmuld/waverunner',
     'enabled': True,
     },

    {'title': 'pyspectre',
     'metadata': {
         'summary': """Parallelizable python front-end for Cadence Spectre simulator.""",
     },
     'url': 'https://gitlab.com/bjmuld/pyspectre',
     'enabled': True,
     }
]
