---
summary: Python extension for reading Cadence binary psf circuit simulation output.
title: libpsf-python


---

# libpsf
=====================
## An easy-to-use Python package for reading Cadence PSF data

Reads waveform, timeseries, AC-analysis, DC, and more kinds of data from proprietary binary Cadence-PSF file format used by Cadence(R) tools and primarily it's Spectre(R) simulator which produce output natively in this format.

Libpsf is easy to use:

    import libpsf
    myobj = libpsf.PSFDataSet( "/path/to/mypsffile.psf" )

and now, 'myobj' makes available the following methods:
  
    myobj.close()
    myobj.invertstruct
    myobj.is_swept()

    myobj.get_header_properties()
    myobj.get_nsweeps()
    myobj.get_signal()
    myobj.get_signal_names()
    myobj.get_signal_properties()
    myobj.get_sweep_npoints()
    myobj.get_sweep_param_names()
    myobj.get_sweep_values()

##### notes:
1. everything is returned as numpy.ndarray type
2. in a transient simulation, time is treated as a "swept" variable:
3. there are psf files for testing in examples/data/

I have spruced up a few things, linked against a newer Boost and adapted the python extension
for use in python 2.7 and 3.4+ environments.

Original contributions by:
- [Henrik Johansen](https://github.com/henjo)
- [Michael Thompson](https://github.com/michaelnt)
- [Taimur Gibran Rabuske](https://github.com/taimurrabuske)

