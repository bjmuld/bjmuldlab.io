---
summary: OpenAI Gym -like interface for ML in circuit simulator environments.
title: circuitgym


---

+# openai-gym-circuits
+A Spectre circuit simulator based implementation of an OpenAI Gym environment
