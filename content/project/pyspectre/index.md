---
summary: Parallelizable python front-end for Cadence Spectre simulator.
title: pyspectre


---

# pyspectre
Python wrapper for Spectre circuit simulator