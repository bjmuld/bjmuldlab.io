---
summary: Songbirt is a lightweight web-based songbook for musicians
title: songbirt


---

#  Songbirt
### a repertoire browser for musical nerds

Songbirt is a lightweight web app for quickly browsing
a songbook and displaying songs during performance.
Songs are individual files (markdown, text, pdf)
stored in your file system.

You can browse quickly by artist and album and display song charts in large and readable formats.

<img src="https://gitlab.com/bjmuld/songbirt/raw/45b69cf85d201443e6d864f05e4a506aeb496745/screencap.png?inline=false" alt="screen cap" width=600 height="360" />



#### Use:
`songbirt /path/to/my/songbook/root` 

will launch the server from that path.

alternately:
```python
$bash>  cd /path/to/my/songbook/
$bash>  songbirt
```

#### Songbook structure:

Songbook directories should be organized like:
```bash
.
|--- artist1/
|   |--- album1/
|   |   |--- song1.md
|   |   |--- song2.text
|   |   |--- song3.pdf
|
|   |--- album2/
|   |   |--- song1.md
|   |   |--- song2.text
|   |   |--- song3.pdf
|
|   |--- song3.md
|   |--- song4.text
|   |--- song5.pdf
|
|--- artist2/
|   |--- album1/
|   |   |--- song1.md
|   |   |--- song2.text
|   |   |--- song3.pdf
|
|   |--- album2/
|   |   |--- song1.md
|   |   |--- song2.text
|   |   |--- song3.pdf
|
|   |--- song3.md
|   |--- song4.text
|   |--- song5.pdf
.
.
.
```