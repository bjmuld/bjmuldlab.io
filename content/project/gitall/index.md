---
summary: CLI tool for performing operations over multiple side-by-side git repos.
title: gitall


---

# Gitall
##  A tool for quickly displaying meta-info about the disposition of multiple git repos

# Example 1:

Project A:  ./common/projectA.git

Project B:  ./common/projectB.git

```bash
> cd common
> gitall status

checking status of all subdirs...
                    dupehawk: items to commit / branch up to date
            iceis-paper-2020: nothing to commit / branch up to date
                xanity-paper: nothing to commit / branch up to date
                      xanity: nothing to commit / branch up to date
                     chonets: nothing to commit / branch behind 
                v4l2-ctl-gui: items to commit / branch up to date
                   pyspectre: nothing to commit / branch up to date
                     experts: nothing to commit / branch behind 
                   quietloud: items to commit / branch behind 
                  waverunner: nothing to commit / branch up to date
                    songbirt: nothing to commit / branch up to date
                      gitall: items to commit / branch up to date
                      btkbdd: nothing to commit / branch up to date
                     anodyne: items to commit / branch up to date
                  circuitgym: nothing to commit / branch up to date
