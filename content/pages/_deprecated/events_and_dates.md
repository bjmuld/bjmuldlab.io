---
title: Conferences


---

Description                                                                                  |  Paper Date   |  Event Date   |
---------------------------------------------------------------------------------------------|---------------|---------------|--
[Information and Communication Tech. for Sustainability (ICT4S)](http://ict4s.org/)          |  01/29/2020   |  06/21/2020   |
[International Joint Conf. on AI (ICJAI)](https://www.ijcai.org/)                            |  02/19/2020   |  08/10/2020   |
[Computational Social Choice Workshop (COMSOC) ](http://research.illc.uva.nl/COMSOC/)        |  03/01/2020   |  06/26/2020   |
[Computing with Limits Workshop (LIMITS) colloc. ICT4S ](https://computingwithinlimits.org)  |  03/27/2020   |  06/21/2020   |
[International Test Conference (ITC) ](http://www.itctestweek.org/)                          |  03/27/2020   |  11/03/2020   |
[Design Automation Conference (DAC) ](https://dac.com/)                                      |  11/21/2020   |  07/19/2020   |
[European Test Symposium (ETS) ](http://www.ieee-ets.org/)                                   |  12/17/2020   |  05/27/2020   |
[Fairness, Accountability, and Transparency (FaccT) (ACM)](https://facctconference.org)      |  08/15/2020   |  01/27/2020   |