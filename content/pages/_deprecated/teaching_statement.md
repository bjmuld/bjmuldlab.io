---
draft: true
title: Statement of Teaching


---

### Motivation {#motivation .unnumbered}

For several reasons, contemporary technical colleges are at risk of
ceding their constructive power, becoming reinforcers of the status-quo
rather than authors of the new. As a professor, I will first and
foremost seek to explore optimistic visions for the future of the
*Practice of Engineering* beyond those defined by the present orthodoxy.
I will inspire a critical understanding of engineering practice while
inspiring students to challenge the casual assertions about "where the
future is heading" and to nourish their imagination of boundless
engineering practice.

#### On "Practical" Engineering Skills {#on-practical-engineering-skills .unnumbered}

The tools of math and science are very potent; we must not merely arm
students but train them in where, when, and why to exercise them.
Consider two trends: a) several measures of life expectancy in the US
have *decreased* for the last three years
[@ariasUnitedStatesLife2019; @ariasUnitedStatesLife2019a; @ariasUnitedStatesLife2018],
and b) use of the phrase "energy efficiency" has exploded, but energy
consumption *increases*
[@worldbankEnergyUseKgoe2019; @worldbankPopulationTotalUnited2019; @GoogleNgramViewer].
These historical realities represent failures -- not of "Science" in the
abstract but of its *practice* -- to deliver the brighter future
technology promises.

The translation of math, science, and engineering theory into concrete
improvement requires skills beyond the strictly technical: social
dynamics, sources of funding, and competing interests are often more
influential on outcome than "the science." If our goal as practitioners
is to be effective, skills and knowledge in these areas should not be
considered orthogonal, but rather, *integral* to Engineering. And so,
awareness and responsiveness to the not-strictly-technical must be
taught concurrently in order to foster well-rounded and effective
practitioners.

### Instructional Themes {#instructional-themes .unnumbered}

#### Nonlinear Dynamics and Complex Systems as Permanent Backdrop {#nonlinear-dynamics-and-complex-systems-as-permanent-backdrop .unnumbered}

I will embrace three major shifts I see underway in the engineering
paradigm: addressing nonlinearity directly, multiscale analyses, and
integrated systems analyses. Nonlinear dynamics and complex systems
integrates these shifts and forms a basis on which to build an
undergraduate engineering education. Firstly, this class of system
represents a superset of traditional engineering education, and
secondly, these kinds of systems reveal many deeper truths about our
world which are lost in abstractions to linearity. The double-pendulum
teaches us that simplicity and complexity coexist; through Lyapunov
time, we confront fundamental limitations to predictive capacity; and
Lotka-Volterra systems illustrate stability, self-repair, and collapse
in systems and structures.

#### Reality-First, Idealized Last {#reality-first-idealized-last .unnumbered}

Classical engineering education begins with the most reduced, separable,
and linear view of problems, leading students to construct and inhabit a
separate, idealized frame for engineering problem solving apart from the
physical world in which they live and absent many of the constraints and
relationships that occupy it. I will *begin* instruction with an
accounting of reality, inventorying the objects and flows we know as
well as the possibility of others we miss. From this vantage, one can
develop more intuition about *why* we make assumptions and abstractions
during analysis without losing sight of the real article. In this way,
we can effectively demystify the nonlinear and the multidimensional,
narrowing the gap between engineering's precision and reality's
fuzziness.

#### Repair and Repurposing in Projects {#repair-and-repurposing-in-projects .unnumbered}

Creative and cooperative hands-on projects have been celebrated and
practiced for some time in undergraduate engineering and have proven
invaluable in many ways. I will carry on this trend: every course will
have a final project, and each will be cooperative, hands-on, and
creative. Additionally, I will attempt to establish repair and
repurposing as central tenets of engineering under the premise that if
we continue to (perhaps unintentionally) emphasize "green field" design
and restrict solution spaces to those which begin at *tabula rasa*, we
all-but-ensure tremendous inefficiency in our societies' resource
management.

### Proposed Course Offerings {#proposed-course-offerings .unnumbered}

#### Engineering The Future {#engineering-the-future .unnumbered}

The course will investigate the proposition that the future is a
deterministic response to forces applied to the present, and as such,
it's the job of scientists and engineers to develop, improve, and
utilize a predictive capacity to engineer a more prosperous, more
democratic, and more vibrant future. The final project will be
team-based and would require the presentation of a probabilistic model
for downstream effects of a technological introduction. Teams will
validate the model on historical data and present predictions for a
contemporary technology.

#### How to Open-Source {#how-to-open-source .unnumbered}

This course is an introduction to the theory, tools, and practice of
open-source engineering. It introduces IP law, licenses, and the ideas
behind open-source and libre technologies; then introduces the tools and
communications commonly used in the community (mailing lists, git, CI,
sphinx, etc.). It culminates with groups surveying projects in a
particular field, doing a deep-dive into one project, and finally making
a contribution to the project (merging original code, etc.).

### Instructional Methods {#instructional-methods .unnumbered}

#### Open Discussions {#open-discussions .unnumbered}

Open discussion fosters curiosity and densely branching analytic thought
and re-frames the lecture topic within the backdrop of a complex,
nonlinear, and interconnecting reality. Classes will begin with a
10-minute casual discussion; topics might include contemporary news
items, philosophy, popular figures, literature, or music with the
implicit goal of identifying relational links to the engineering subject
at hand. I have found that professors who embrace open discussion in
this way build legacies of enrichment and strong relationships with
students.

#### Student-Led Learning {#student-led-learning .unnumbered}

In order to review homework, I will select interesting problems and
invite volunteers to transcribe their solutions on the board. Together,
the class will discuss the volunteers' solutions, and they will
implicitly have to defend the analytical decisions they made along the
way. This builds individual confidence while providing a focus on the
sharing of knowledge and collaboration. Because it presents
opportunities to celebrate creative and insightful processes, it is also
a useful way to prevent formation of orthodoxies of thought.

#### Material-Value Engineering {#material-value-engineering .unnumbered}

In order to more adequately incorporate the economics of materials,
resources, and energy, projects will include not only conventional
dollar and power-budget requirements, but also mineral budgets (ie. mass
of copper and lithium) and budgets on so-called "virgin materials" in
attempt to better capture true cost of materials. Students will be
encouraged to salvage, repair, and repurpose materials rather than
procure new items from COTS suppliers.
