---
draft: true
title: Statement of Research


---

### Motivating Themes {#motivating-themes .unnumbered}

#### From "Interdisciplinary" to Integrated Science {#from-interdisciplinary-to-integrated-science .unnumbered}

Many challenges we humans face arise from the vacuum between silos of
expertise blinded by domain-specificity; I believe we must work hard to
rapidly close the space between single-domain silos by encouraging
communication and mixing of ideas, by sharing our expertise without
reluctance, and by proactively re-coupling ideas and processes that were
decoupled in the past. Siloed, we advance linearly; networked, we
advance exponentially.

#### Open-Source {#open-source .unnumbered}

I am an advocate and active member of the open-source project. I view
the Linux operating system as a very successful proof-of-concept for a
new organizational model. Founded on voluntary participation and willful
liberation of "intellectual property," it's led to the development of
exponentially more *Free* tools which in turn have enabled an
immeasurable volume of scientific discovery. Open-source tools are the
central model for the next era in science, and so I maintain a
commitment to their development and use.

### Proposed Long-Horizon Research (5$+$ years) {#proposed-long-horizon-research-5-years .unnumbered}

#### Complexity Science {#complexity-science .unnumbered}

I will pursue the study of complex and multi-scale dynamics in all kinds
of human-designed systems. Relational links between technology, social
systems, and the environment will inform both the maintenance of
existing technology and the design of the new. Just as we are seeing
low-level interactions confound the long-term viability of VLSI design
paradigms, so too will we see limitations in the scaling and continued
utility of corporate management strategies, global supply-chain systems,
and structures of governance.

Studies of scale, networks, and complexity have recently brought us
concepts like chaotic mixing, emergence, and adaptation, and there
remain large opportunities in the fields of applied complexity and
complex physical dynamics (as opposed to neuronal dynamics). Already,
there are many calls from the NSF and DARPA for work exploring causal
relationships between complex social systems, technology (AI/ML), and
the environment, and I believe agency funding will continue to abound.

#### Abstraction Science {#abstraction-science .unnumbered}

A close relative of complexity, abstraction has enabled many of the
scientific and technological paradigms of the 20th century. We know its
utility; however, we have limited scientific understanding of its
limitations and quantitative understanding of its mechanisms. I will
study information and energy flows across abstraction boundaries both in
extant natural and man-made physical systems and in theoretical, toy
structures. My work in designing, testing, and diagnosing failures in
complex electrical systems -- across scales, across contexts, and across
abstractions -- has given me unique insight into the relationships
between abstraction layers, and I'm very excited to study the phenomena
taking place at their boundary.

### Mid-Term Research (1-5 years) {#mid-term-research-1-5-years .unnumbered}

#### Paradigms in Design Automation {#paradigms-in-design-automation .unnumbered}

We are simultaneously on the cusp of demand for EDA tools for designing
and assembling quantum systems and at a stand-still in CMOS integration
scale (not just device shrinking, but also system growth). Both present
opportunities for new ideas in design synthesis, design automation, and
for open-source approaches. My expertise in abstraction and complexity
in mixed-signal VLSI design places me in a unique position to contribute
to the next-generation of system integration and design tools. I
anticipate starting this work in the coming weeks with Prof. Jennifer
Hasler as a post-doc. I will leverage relationships at Microsoft
Quantum, Sydney where there is significant interest in this work,
providing a strong industry liaison and future funding possibilities.

#### Process Variability as a Feature {#process-variability-as-a-feature .unnumbered}

I will explore applications for magnifying and leveraging process
variability among manufactured elements. Such paradigms are anathema to
conventional design but open entirely new possibilities. One can
envision hardware accelerators, for example, in which arrays of
process-varied subsystems could produce alternate hypotheses or emulate
"multi-start" techniques in parallel in real-time. As for uniqueness, I
have participated in exploratory work in the design of analog circuits
used for identity authentication, and there are low-hanging fruit to be
gleaned through careful application of fundamental cryptographic
principals in design of analog physical-unclonable-functions. Work in
PUFs is vibrantly supported through programs in cybersecurity,
hardware-security, and the like. I will leverage existing relationships
with Ulrich Rührmair and Ilia Polian at U.Conn/Ruhr-Universitat and U.
Stuttgart and seek funding from programs like the NSF's Secure and
Trustworthy Cyperspace program.

### Immediate-Term Research (Tomorrow) {#immediate-term-research-tomorrow .unnumbered}

#### Diagnosis in Highly Abstracted, Nonlinear Systems {#diagnosis-in-highly-abstracted-nonlinear-systems .unnumbered}

Having already introduced novel techniques for the autonomous
formulation of circuit diagnoses, I will leverage recent work in
generative modeling for rapid synthesis of fault hypotheses, and will
pursue techniques for quickly paring hypothesis-trees. I will also
generalize the approach so that it can be applied universally, to
aeronautical and wind-turbine systems, for example. I have a good
relationship with David Yeh at The Semiconductor Research Corporation
(SRC) and he has repeatedly expressed interest in pursuing this line of
work. I anticipate both NSF and DARPA, given their recent interest in
funding work in "digital twins," would be eager to participate as well.

#### Active (Semi-supervised) Machine Learning {#active-semi-supervised-machine-learning .unnumbered}

Recently, work in so-called "active learning (AL)" has drawn much
attention within the machine learning community. It centers on the
efficient algorithmic selection of data to be labeled. From my early
work developing the first algorithms for automatic *online* test
synthesis for analog systems to my more recent work in diagnosis, I have
understood data labeling to be both ambiguous and a very high-cost
proposition in many contexts. Because the class of problem *without*
easily collected and labeled data-sets is likely the more prevalent, I
foresee a brighter future in active learning than in supervised
learning. Because many of my techniques have been published in
literature with a focus on VLSI system testing, there are several
immediate opportunities for generalizing learning mechanisms and
engaging the broader machine learning, AI, and "deep learning"
communities. Subsequently, I will explore methods and applications for
active learning techniques outside of EDA.
