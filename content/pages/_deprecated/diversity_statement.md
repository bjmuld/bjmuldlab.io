---
draft: true
title: Statement of Diversity


---

My understanding of diversity stems from my having walked a winding road
in life, at times by choice and at times by circumstance. Having
occupied several positions atypical for an academician, I hold an
outside perspective on many issues and retain bonds to diverse
communities outside the academy. I have seen that increased diversity
leads to more just and more democratic environments, and so I am
committed to the inclusion of all people in technology's creation and
exercise. My contributions come in the forms of: instructing around the
principles of inclusion, responsibility and accountability for
engineering outcomes, organizing student-led community repair clinics,
and increasing community access to creative "hackerspaces."

#### Experiences with Diversity {#experiences-with-diversity .unnumbered}

I did not grow up "of means;" my mother was disabled, and my father was
forever in the process of exchanging his blue collar for a white one. In
fact, it was only through his negotiations and pleading that I was able
to attend private grade school on scholarship. Were it not for this
gift, it's quite possible that few of my talents for math and science
would have been developed.

In private school, my peers' families owned companies; my dad was often
"between jobs." They lived in historic homes; my dad lived in a
historically poor, black neighborhood. And so, I learned to make friends
of all kinds and to find commonality among a diversity of peers. With my
private school friends, I rode bikes and played video games; and with my
neighborhood friends, I rode (older) bikes and played (older) video
games.

Later in life, I dropped out of college to pursue a career in music. I
worked a day job at a print shop, weekends at the aquarium, and nights
among Grammy-winning artists in a recording studio. I had roommates,
band-mates, and friends from all walks of life. In these social circles,
I was consistently reminded of the universality of intelligence,
insight, and ability. At the same time, I could easily contrast the
fortunes of that group to those of my grade-school friends who were
stepping into management positions, board seats, and the like without so
much as an MBA.

Without sufficient diversity in the mechanisms of our meritocracy, it
fails, and vast groups are denied access to opportunity, education,
employment and comfort. Having seen the vast potential in these
historically under-served groups throughout my life, I'm determined to
serve and include them in every way I can.

#### Diversity and Technology {#diversity-and-technology .unnumbered}

Representative diversity's role in technological outcomes is well
documented. One example is the much publicized failure of HP's and
Google's facial recognition algorithms to recognize people-of-color.
Democratic and just technology can only arise from democratic and just
environments, and those kinds of environments are achieved by actively
pushing and challenging orthodoxies.

I am committed to serving minorities in engineering and will actively
recruit and conduct research with members of underrepresented groups in
engineering. I optimistically believe that measures are underway to
address the lack representational diversity in our hierarchies, and
through continued examination and pressure, we can achieve justice and
democracy in our institutions and in our technology.

#### Healthy Heterodoxy {#healthy-heterodoxy .unnumbered}

Myopic modes of thinking pose a tremendous threat to justice and
democracy and arise naturally from environments which fail to value
diversity. For the moment, technology's exercise is not a diverse or
democratic activity. Human beings generally have a great distaste of
phones and computers: we measure developmental harm in our children, see
decreased interpersonal engagement, and we are afraid AI jeopardizes our
livelihood. Yet, there is hardly a trace of these concerns in serious
engineering settings.

I'm determined to foster a more democratic and inclusive practice of
technology. I will be instructive about the duty of technologists to
defer to the public, will introduce and welcome all to science and
technology, and will build institutional bridges into non-technical
communities for bilateral communication and expression.

### Opportunities to Increase Community Diversity {#opportunities-to-increase-community-diversity .unnumbered}

#### Repair Clinics {#repair-clinics .unnumbered}

Repair is overlooked institutionally, is considered unglamorous
culturally, and is under attack in courts. Repair, however is the first
line of defense for a society seeking to reduce waste and increase its
resource efficiency. One organization in which I participate has had
great success running monthly "brake light repair clinics." I see an
immediate opportunity for engineering students to expand this model to
computers, small engines, appliances, and potentially moderate
automotive repairs with the goals of: 1) providing a very potent and
necessary resource to communities that need it most, 2) acting as a
mechanism for introducing young community members to engineering, and 3)
teaching engineering students the value and necessity of repair.

#### Hacking for All {#hacking-for-all .unnumbered}

In the last few years, we have seen the proliferation of "makerspaces"
and "hackerspaces" in both popular and academic settings. In many
settings, these spaces have become capital-intensive arenas for
commercial participation making them less replicable, less accessible,
and less useful. Participants' creative abilities are less challenged
when they can rely on external provision of machines and materials. This
new form is in direct contradiction to the hacking and "DIY" ethics from
which it came, potentially undermining the exciting and creative aspects
which drew so much attention in the first place. I will supervise a
space for all -- students as well as community members -- where salvaged
and scrapped items can be repaired, repurposed, modified, and reborn as
useful and exciting expressions of the imagination and where creative
problem solving is employed by necessity.
