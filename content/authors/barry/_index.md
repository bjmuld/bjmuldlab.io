---
# Display name
name: Barry Muldrey, Ph.D.

# Username (this should match the folder name)
authors:
- barry

# Is this the primary user of the site?
superuser: true

# Role/position
role: Senior Analog and Mixed-Signal Engineer, Intel Corp.

# Organizations/Affiliations
organizations:
- name: Intel Corp.
  url: "https://www.intel.com/"
- name: University of Mississippi
  url: "https://ftl.ece.olemiss.edu/"
- name: Georgia Institute of Technology
  url: "https://lars.gatech.edu/"

# Short bio (displayed in user profile at end of posts)
bio: Highest-Level Design Insights

interests:
- Analog Verification
- Circuit Validation
- Design Testing
- Artificial Intelligence (Active Learning)
- Analog Computing

education:
  courses:
  - course: Ph.D. in Electrical and Computer Engineering
    institution: Georgia Institute of Technology
    year: 2019
  - course: M.S. in Electrical and Computer Engineering
    institution: Georgia Institute of Technology
    year: 2014
  - course: B.S. in Electrical Engineering
    institution: University of New Orleans
    year: 2009

# Social/Academic Networking
# For available icons, see: https://sourcethemes.com/academic/docs/page-builder/#icons
#   For an email link, use "fas" icon pack, "envelope" icon, and a link in the
#   form "mailto:your-email@example.com" or "#contact" for contact widget.
social:

- icon: envelope
  icon_pack: fas
  link: '#contact'  # For a direct email link, use "mailto:test@example.org".

#- icon: twitter
#  icon_pack: fab
#  link: https://twitter.com/GeorgeCushen

- icon: orcid
  icon_pack: ai
  link: https://orcid.org/0000-0003-2052-6096

- icon: google-scholar
  icon_pack: ai
  link: https://scholar.google.com/citations?user=PvOVtwsAAAAJ&hl=en

- icon: gitlab
  icon_pack: fab
  link: https://gitlab.com/bjmuld

- icon: dblp
  icon_pack: ai
  link: https://dblp.uni-trier.de/pers/hd/m/Muldrey:Barry_John

# Link to a PDF of your resume/CV from the About widget.
# To enable, copy your resume/CV to `static/files/cv.pdf` and uncomment the lines below.
- icon: cv
  icon_pack: ai
  link: https://gitlab.com/bjmuld/resume/-/raw/master/data-sw-focus/muldrey_resume_dsm.pdf?inline=true
#  link: https://gitlab.com/bjmuld/resume/-/raw/master/muldrey_resume_xFunc_teams_hardware.pdf?inline=true

# Enter email to display Gravatar (if Gravatar enabled in Config)
email: "barry@muldrey.net"

# Organizational groups that you belong to (for People widget)
#   Set this to `[]` or comment out if you are not using People widget.
user_groups:
- Researchers
#- Visitors

---

Barry Muldrey is an expert AI/ML circuit-design-analysis and EDA executioner :stuck_out_tongue:

---


[Read his CV (.PDF)](https://gitlab.com/bjmuld/resume/-/raw/master/data-sw-focus/muldrey_resume_dsm.pdf?ref_type=heads&inline=true)

<!--Barry grew up in New Orleans where he attended Jesuit High School.
In 2003, Barry began attending the University of Southern California on academic
scholarship where he pursued the study of jazz and studio guitar performance.
Returning to New Orleans after a year to work under Mark Binghamand John Fischbach at
Piety Street Recording studios, a closet of broken electronics piqued his interest,
and he began tinkering with electronics.
Hurricane Katrina threw the city into disarray and prompted Barry’s return to 
academics when he enrolled in the University of New Orleans’ electrical
engineering program.
There he was fortunate to conduct research in neural networks and participate in team robotics under the guidance
of Drs. Edit Bourgeois and Dimitrios Charalampidis.-->

Did you know?

+ Barry had a short first-career in the music industry, working as a recording engineer and occasionally producer and studio musician.

+ While finishing his undergraduate, Barry launched a startup music recording technology company.

+ Barry holds a Ph.D. from the Georgia Institute of Technology where he researched novel applications of AI/ML to circuit model validation, model debugging, post-Si model validation, and automated testing of mixed-signal systems.

+ From 2019 to 2021, Barry was a junior professor at the University of Mississippi where he co-founded a B.S. Computer Engineering curriculum and degree program. There, he created several novel courses which gave students in-depth and hands-on experience with mobile phone SoC firmware design and hybrid IoT cloud system design and deployment.

+ From 2021 to the present, Barry is a Senior Engineer in Intel's Server DDR (Analog IP) group where he enables advanced circuit and system simulation as well as conducts multi-abstraction gigascale-data analysis.

+ Barry’s hobbies include playing, recording, and performing music, woodcraft, and welding.

<!--{{% refcheck "pages/research_statement.md" "Statement of Research" %}}
{{% refcheck "pages/teaching_statement.md" "Teaching Statement" %}}
{{% refcheck "pages/diversity_statement.md" "Statement on Diversity" %}}-->
