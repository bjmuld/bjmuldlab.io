---
title: "Self-Learning MIMO-RF Receiver Systems: Process Resilient Real-Time Adaptation to Channel Conditions for Low Power Operation"
date: 2014-11-01
publishDate: 2020-03-20T10:54:18.202131Z
authors: ["D. Banerjee", "B. Muldrey", "S. Sen", "X. Wang", "A. Chatterjee"]
publication_types: ["1"]
abstract: "Prior research has established that dynamically trading-off the performance of the RF front-end for reduced power consumption across changing channel conditions, using a feedback control system that modulates circuit and algorithmic level \"tuning knobs\" in real-time, leads to significant power savings. It is also known that the optimal power control strategy depends on the process conditions corresponding to the RF devices concerned. This complicates the problem of designing the feedback control system that guarantees the best control strategy for minimizing power consumption across all channel conditions and process corners. Since this problem is largely intractable due to the complexity of simulation across all channel conditions and process corners, we propose a self-learning strategy for adaptive MIMO-RF systems. In this approach, RF devices learn their own performance vs. power consumption vs. tuning knob relationships \"on-the-fly\" and formulate the optimum reconfiguration strategy using neural-network based learning techniques during real-time operation. The methodology is demonstrated for a MIMO-RF receiver front-end and is supported by hardware validation leading to 2.5X power savings in minimal learning time."
featured: false
publication: "*2014 IEEE/ACM International Conference on Computer-Aided Design (ICCAD)*"
tags: ["Radio frequency", "Tuning", "learning (artificial intelligence)", "Receivers", "Artificial neural network", "Real-time systems", "LNA", "RF devices", "Adaptation", "feedback control system", "Low Power", "low power operation", "MIMO", "MIMO communication", "Mixer", "neural-network based learning techniques", "OFDM", "optimal power control strategy", "Optimized production technology", "Power demand", "radio receivers", "Receiver. Radio-Frequency", "reduced power consumption", "resilient real-time adaptation", "Self-learning", "self-learning MIMO-RF receiver systems"]
doi: "10.1109/ICCAD.2014.7001430"
---

