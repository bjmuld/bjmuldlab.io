---
title: "Atomic Model Learning: A Machine Learning Paradigm for Post Silicon Debug of RF/Analog Circuits"
date: 2014-04-01
publishDate: 2020-03-20T10:54:18.202803Z
authors: ["S. Deyati", "B. J. Muldrey", "A. Banerjee", "A. Chatterjee"]
publication_types: ["1"]
abstract: "As RF design scales to the 28nm technology node and beyond, pre-silicon simulation and verification of complex mixed-signal/RF SoCs is becoming intractable due to the difficulties associated with simulating diverse electrical effects and design bugs. As a consequence, there is increasing pressure to develop comprehensive post-silicon test and debug tools that can be used to identify design bugs and improve modeling of complex electrical nonidealities observed in silicon. Often, it is not known a-priori what these bugs are and how they can be modeled, significantly complicating the debug process. In this research, a new atomic model learning approach is proposed that uses supervised learning techniques to diagnose design bugs and learn unknown module-level behaviors. Nonideality modeling artifacts called model atoms are inserted into different nodes of the design signal flow paths to learn unknown behaviors along those paths. Under the assumption that the design bug is localized, it is shown that the source of the bug can be identified with high resolution even when the nature of the bug is unknown. The method has been applied to a conventional wireless as well as a polar radio transmitter and key results that demonstrate usefulness and feasibility of the proposed approach are presented."
featured: false
publication: "*2014 IEEE 32nd VLSI Test Symposium (VTS)*"
tags: ["Integrated circuit modeling", "analog circuits", "analogue integrated circuits", "Fault diagnosis", "integrated circuit design", "Radio frequency", "integrated circuit modelling", "Tuning", "Artificial neural networks", "learning (artificial intelligence)", "Mixers", "radiofrequency integrated circuits", "RF circuits", "electronic engineering computing", "Mathematical model", "Power amplifiers", "Model checking", "supervised learning", "mixed-signal SoC", "RF SoC", "atomic model learning", "design bug diagnosis", "machine learning paradigm", "module level behavior", "post silicon debug", "Preamplifiers", "Radio transmitters"]
doi: "10.1109/VTS.2014.6818791"
---

