---
title: "Concurrent Multi-Channel Crosstalk Jitter Characterization Using Coprime Period Channel Stimulus"
date: 2016-01-01
publishDate: 2020-03-20T13:17:38.013711Z
authors: ["Nicholas L. Tzou", "Debesh Bhatta", "Xian Wang", "Te-Hui Chen", "Sen-Wen Hsiao", "Barry Muldrey", "Hyun Woo Choi", "Abhijit Chatterjee"]
publication_types: ["2"]
abstract: "In recent years, the use of highly parallelized and high-speed data links has exacerbated the problem of crosstalk coupling. Signals with high frequency components couple to and degrade the quality of signals in adjacent lines aggressively with reduced device dimensions. In this paper, we propose a methodology for characterizing multiple crosstalk jitter effects in the time domain using a single data capture without the use of any channel models. The method uses repetitive data patterns across different signal lines with coprime periods. Each signal with crosstalk effects is digitized using sub-Nyquist sampling, and after back-end digital signal processing, the original transmitted signal on each line is isolated from its crosstalk components. Mathematical analysis shows that only by using patterns with coprime lengths, unbiased crosstalk characterization is possible. Hardware measurements support the proposed test methodology."
featured: false
publication: ""
tags: ["Noise measurement", "mathematical analysis", "time-domain analysis", "sampling methods", "time domain", "crosstalk", "circuits", "components", "devices and systems", "components; circuits; devices and systems", "back-end digital signal processing", "bit error rate", "Bit error rate", "bounded uncorrelated jitter", "Bounded uncorrelated jitter", "channel models", "concurrent multichannel crosstalk jitter characterization", "coprime bit sequence", "coprime lengths", "coprime period channel stimulus", "coprime periods", "couplings", "crosstalk components", "crosstalk coupling", "crosstalk jitter", "data handling", "hardware measurements", "high-speed data links", "histograms", "Histograms", "jitter", "jitter separation", "multiple crosstalk jitter effects", "reduced device dimensions", "repetitive data patterns", "single data", "subNyquist sampling", "unbiased crosstalk characterization"]
doi: "10.1109/TCSI.2016.2537898"
---

