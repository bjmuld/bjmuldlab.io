---
title: "Clustering Using Multilayer Perceptrons"
date: 2009-01-01
publishDate: 2020-03-20T13:17:38.013502Z
authors: ["Dimitrios Charalampidis", "Barry Muldrey"]
publication_types: ["2"]
abstract: "In this paper we present a multilayer perceptron-based approach for data clustering. Traditionally, data clustering is performed using either exemplar-based methods that employ some form of similarity or distance measure, discriminatory function-based methods that attempt to identify one or several cluster-dividing hyper-surfaces, point-by-point associative methods that attempt to form groups of points in a pyramidal manner by directly examining the proximity between pairs of points or groups of points, and probabilistic methods which assume that data are sampled from mixture distributions. Commonly, in exemplar-based methods, each cluster is represented by a multi-dimensional centroid. In this paper, we explore the function approximation capabilities of multilayer perceptron neural networks in order to build exemplars which are not simply points but curves or surfaces. The proposed technique aims to group data points into arbitrary-shaped point clouds. The proposed approach may exhibit problems similar to other traditional exemplar-based clustering techniques such as k -means, including convergence to local minimum solutions with respect to the cost function. However, it is illustrated in this work that approaches such as split-and-merge can be appropriately adjusted and employed in the proposed technique, in order to alleviate the problem of reaching poor local minimum solutions."
featured: false
publication: ""
tags: ["Neural Networks", "68t99", "Clustering", "Multilayer Perceptrons"]
doi: "10.1016/j.na.2009.06.064"
---

