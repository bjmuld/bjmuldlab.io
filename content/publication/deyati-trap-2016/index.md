---
title: "TRAP: Test Generation Driven Classification of Analog/RF ICs Using Adaptive Probabilistic Clustering Algorithm"
date: 2016-01-01
publishDate: 2020-03-20T10:54:18.201247Z
authors: ["S. Deyati", "B. J. Muldrey", "A. Chatterjee"]
publication_types: ["1"]
abstract: "In production testing of analog/RF ICs, application of standard specification-based tests for IC classification is not always an attractive option due to the high costs and test times involved. In this paper, a new test generation algorithm for IC classification is first developed that has the property that the corresponding DUT response signatures for devices from diverse process corners are maximally separable. In other words, the process space can be partitioned into a maximally large number of partitions and each tested IC can be uniquely diagnosed to lie in one such partition from its response to the applied test stimulus. Boolean zed representations of analog/RF circuits are used to facilitate rapid test stimulus generation. Next, the responses of ICs tested in production to the applied classification test are used to adaptively classify the ICs into clusters of \"good\", \"bad\" and \"marginal\" devices (TRAP). This is done through the use of probabilistic neural networks that do not require complete network retraining every time an IC with different input-output behavior is observed. The classification test applied helps in determining if a new device being tested is from a different process corner than encountered before, thereby aiding IC classification. Simulation experiments show that the learning process is rapid and minimizes device misclassification with significant savings in test time."
featured: false
publication: "*2016 29th International Conference on VLSI Design and 2016 15th International Conference on Embedded Systems (VLSID)*"
tags: ["Integrated circuit modeling", "analogue integrated circuits", "Circuit testing", "integrated circuit testing", "Standards", "testing", "Training", "probability", "Automatic test pattern generation", "radiofrequency integrated circuits", "Mathematical model", "production testing", "analog IC", "adaptive probabilistic clustering algorithm", "Classification algorithms", "probabilistic neural networks", "RF IC", "test generation driven classification", "TRAP"]
doi: "10.1109/VLSID.2016.118"
---

