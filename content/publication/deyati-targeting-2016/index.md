---
title: "Targeting Hardware Trojans in Mixed-Signal Circuits for Security"
date: 2016-07-01
publishDate: 2020-03-20T10:54:18.200187Z
authors: ["S. Deyati", "B. J. Muldrey", "A. Chatterjee"]
publication_types: ["1"]
abstract: "The proliferation of third-party silicon manufacturing has increased the vulnerability of integrated circuits to malicious insertion of hardware for the purpose of leaking secret information or even rendering the circuits useless while deployed in the field. A key goal is to detect the presence of such circuits before they are activated for subversive reasons. One way to achieve this is to detect the presence of parasitic loads on internal nodes of a victim circuit. However, such detection becomes difficult in the presence of normal process variations of the silicon manufacturing process itself. In this work, we show how high-resolution detection of parasitic loads on internal circuit nodes can be achieved using a combination of test stimulus design and design-for-Trojan detection techniques. We illustrate our ideas on digital as well as analog/mixed-signal circuits and point to directions for future research."
featured: false
publication: "*2016 IEEE 21st International Mixed-Signal Testing Workshop (IMSTW)*"
tags: ["analog circuits", "analogue integrated circuits", "Digital circuits", "Hardware", "mixed analogue-digital integrated circuits", "mixed-signal circuits", "hardware Trojan detection", "invasive software", "security", "Trojan horses", "elemental semiconductors", "Si", "silicon", "Integrated circuits", "digital integrated circuits", "Inverters", "integrated circuit manufacture", "Capacitance", "Delays", "design-for-Trojan detection", "hardware intrusion detection", "Hardware security", "high-resolution detection", "internal circuit nodes", "parasitic loads", "side channel analysis", "test stimulus design", "third-party manufacturing"]
doi: "10.1109/IMS3TW.2016.7524238"
---

