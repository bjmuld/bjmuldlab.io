---
title: "Concurrent Built in Test and Tuning of Beamforming MIMO Systems Using Learning Assisted Performance Optimization"
date: 2017-01-01
publishDate: 2020-03-20T10:54:18.204099Z
authors: ["Sabyasachi Deyati", "Barry J. Muldrey", "Byunghoo Jung", "Abhijit Chatterjee"]
publication_types: ["1"]
abstract: ""
featured: false
publication: "*2017 IEEE International Test Conference (ITC)*"
---

