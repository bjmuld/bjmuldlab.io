---
title: "Challenge Engineering and Design of Analog Push Pull Amplifier Based Physically Unclonable Function for Hardware Security"
date: 2015-11-01
publishDate: 2020-03-20T10:54:18.203423Z
authors: ["S. Deyati", "B. J. Muldrey", "A. D. Singh", "A. Chatterjee"]
publication_types: ["1"]
abstract: "In the recent past, Physically Unclonable Functions (PUFs) have been proposed as a way of implementing security in modern ICs. PUFs are hardware designs that exploit the randomness in silicon manufacturing processes to create IC-specific signatures for silicon authentication. While prior PUF designs have been largely digital, in this work we propose a novel PUF design based on transfer function variability of an analog push-pull amplifier under process variations. A differential amplifier architecture is proposed with digital interfaces to allow the PUF to be used in digital as well as mixed-signal SoCs. A key innovation is digital stimulus engineering for the analog amplifier that allows 2X improvements in the uniqueness of IC signatures generated over arbiter-based digital PUF architectures, while maintaining high signature reliability over +/- 10 % voltage and -20 to 120 degree Celsius temperature variation. The proposed PUF is also resistive to model building attacks as the internal analog operation of the PUF is difficult to reverse-engineer due to the continuum of internal states involved. We show the benefits of the proposed PUF through comparison with a traditional arbiter-based digital PUF using simulation experiments."
featured: false
publication: "*2015 IEEE 24th Asian Test Symposium (ATS)*"
tags: ["system-on-chip", "Hardware", "mixed analogue-digital integrated circuits", "Manufacturing processes", "security", "Security", "Clustering algorithms", "Mathematical model", "Reliability", "transfer functions", "Buildings", "Hardware Security", "hardware security", "mixed-signal SoC", "analog push pull amplifier", "arbiter-based digital PUF architectures", "asynchronous circuits", "differential amplifier", "differential amplifiers", "digital stimulus engineering", "IC signatures", "IP Protection", "model building attacks", "physically unclonable function", "Physically Unclonable Function", "silicon authentication", "systems-on-chips", "temperature -20 degC to 120 degC", "transfer function variability"]
doi: "10.1109/ATS.2015.29"
---

