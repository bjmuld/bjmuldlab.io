---
title: "Post-Silicon Validation: Automatic Characterization of RF Device Nonidealities Via Iterative Learning Experiments on Hardware"
date: 2016-01-01
publishDate: 2020-03-20T10:54:18.200520Z
authors: ["Barry Muldrey", "Sabyasachi Deyati", "Abhijit Chatterjee"]
publication_types: ["1"]
abstract: "Recent studies show that increasing numbers of design bugs are escaping to post-silicon due to the complexity of advanced designs and the lack of adequate verification tools that can validate complex electrical interactions between electrical subsystems on an integrated circuit. In this paper, we present a novel tool for post-silicon validation of mixed-signal/RF cir- cuits through cooperative test stimulus generation and behavior- learning. The implemented technique leverages iterative super- vised learning techniques to comprehensively diagnose anomalies between the input-output behavior of the silicon device and the corresponding behavior predicted by its reference model. This results in both a more efficient validation test stimulus and an improved behavioral model which captures non-ideal silicon behaviors. Preliminary results on RF devices prove the feasibility of the proposed validation methodology."
featured: false
publication: ""
---

