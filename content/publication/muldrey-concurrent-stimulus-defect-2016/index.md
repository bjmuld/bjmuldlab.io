---
title: "Concurrent Stimulus and Defect Magnitude Optimization for Detection of Weakest Shorts and Opens in Analog Circuits"
date: 2016-11-01
publishDate: 2020-03-20T10:54:18.202616Z
authors: ["B. Muldrey", "S. Deyati", "A. Chatterjee"]
publication_types: ["1"]
abstract: "We present a methodology for algorithmic generation of test signals for thedetection and diagnosis of a variety of short and open-circuit defects in analogcircuits. Prior algorithms have focused on test generation for known short oropen defect values. This places the burden of failure coverage on accurateanalysis of observed defects in known failed parts at high cost. In this work, we optimize the test stimulus to detect the it weakest shorts and opens inanalog circuits using a concurrent stimulus and defect value optimizationalgorithm. Since the defect value itself is an optimization parameter, the responses of nonlinear circuits corresponding to it multipledefect values are considered as opposed to a it single linearizedrepresentation corresponding to a fixed defect value as in the existing state ofthe art. The algorithm produces a test stimulus along with the values of theweakest shorts and opens that the stimulus can detect (the locations of thedefects are specified to the algorithm). These values are determined by thedesign of the analog circuit itself and therefore subsume all specifieddetectable defects for the circuits concerned. Experimental results show thefeasibility of the proposed approach on selected test cases and defect sets."
featured: false
publication: "*2016 IEEE 25th Asian Test Symposium (ATS)*"
tags: ["Integrated circuit modeling", "analog circuits", "Circuit faults", "fault detection", "testing", "mixed-signal", "Algorithm design and analysis", "optimization", "Trojans", "Capacitance", "Heuristic algorithms", "Analog", "Defect detection"]
doi: "10.1109/ATS.2016.61"
---

