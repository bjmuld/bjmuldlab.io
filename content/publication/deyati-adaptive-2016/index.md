---
title: "Adaptive Testing of Analog/RF Circuits Using Hardware Extracted FSM Models"
date: 2016-04-01
publishDate: 2020-03-20T10:54:18.202994Z
authors: ["S. Deyati", "B. J. Muldrey", "A. Chatterjee"]
publication_types: ["1"]
abstract: "The test generation problem for analog/RF circuits has been largely intractable due to the fact that repetitive circuit simulation for test stimulus optimization is extremely time-consuming. As a consequence, it is difficult, if not impossible, to generate tests for practical mixed-signal/RF circuits that include the effects of tester inaccuracies and measurement noise. To offset this problem and allow test generation to scale to different applications, we propose a new approach in which FSM models of mixed-signal/RF circuits are abstracted from hardware measurements on fabricated devices. These models allow accurate simulation of device behavior under arbitrary stimulus and thereby test stimulus generation, even after the device has been shipped to a customer. As a consequence, it becomes possible to detect process shifts with fine granularity and regenerate tests to adapt to process perturbations in a dynamic manner without losing test accuracy. A complete methodology for such adaptive testing of mixed-signal/RF circuits is developed in this paper. Simulation results and hardware measurements are used to demonstrate the efficacy of the proposed techniques."
featured: false
publication: "*2016 IEEE 34th VLSI Test Symposium (VTS)*"
tags: ["circuit simulation", "Integrated circuit modeling", "analogue integrated circuits", "Circuit testing", "Computational modeling", "integrated circuit testing", "circuit optimisation", "Radio frequency", "testing", "Testing", "Hardware", "mixed analogue-digital integrated circuits", "Automatic test pattern generation", "finite state machines", "Detectors", "radiofrequency integrated circuits", "integrated circuit measurement", "measurement noise", "Classification algorithms", "process perturbations", "hardware measurements", "Adaptation models", "adaptive testing", "analog-RF circuits", "hardware extracted FSM models", "measurement errors", "measurement uncertainty", "mixed-signal-RF circuits", "test generation problem", "test stimulus generation", "test stimulus optimization", "tester inaccuracies"]
doi: "10.1109/VTS.2016.7477283"
---

