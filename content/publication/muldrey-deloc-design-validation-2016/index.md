---
title: "DE-LOC: Design Validation and Debugging under Limited Observation and Control, Pre- and Post-Silicon for Mixed-Signal Systems"
date: 2016-11-01
publishDate: 2020-03-20T10:54:18.203191Z
authors: ["B. Muldrey", "S. Deyati", "A. Chatterjee"]
publication_types: ["1"]
abstract: "In the modern mixed-signal SoC design cycle, designers are frequently tasked with detecting and diagnosing behavioral discrepancies between design descriptions given at different levels of hierarchy, e.g. behavioral vs. transistor level descriptions or behavioral/transistor level descriptions vs. fabricated silicon. One problem is detection, to determine if behavioral differences between design descriptions exist. If such differences (anomalies) are detected, then diagnosis is concerned with identifying the module in a hierarchical design description of the system that is most likely the root cause of the anomaly (typically under the constraint that only the primary outputs of the top-level hierarchies are observed. Previously proposed machine-learning classifiers require prior knowledge about the kinds of likely design errors typically encountered. In this work, we present a novel technique for the algorithmic foundation of circuit diagnosis predictions which does not require any assumptions about the nature of design errors. Our method employs iterative and alternate on-the-fly test generation and least-squares fitting of embedded low-order nonlinear filters to produce a best-guess estimate of the root cause of the anomaly. Experiments are conducted on two test vehicles, an RF transceiver and a phase-locked loop, several bug models are implemented, and the system's diagnosis predictions are analyzed."
featured: true
publication: "*2016 IEEE International Test Conference (ITC)*"
tags: ["Circuit faults", "fault detection", "testing", "Training", "Computer bugs", "mixed-signal", "Predictive models", "Debugging", "silicon", "Algorithm design and analysis", "Analog", "Defect detection", "diagnosis"]
doi: "10.1109/TEST.2016.7805868"
---

