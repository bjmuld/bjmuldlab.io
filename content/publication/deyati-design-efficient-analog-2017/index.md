---
title: "Design of Efficient Analog Physically Unclonable Functions Using Alternative Test Principles"
date: 2017-01-01
publishDate: 2020-03-20T10:54:18.203906Z
authors: ["Sabyasachi Deyati", "Barry Muldrey", "Adit Singh", "Abhijit Chatterjee"]
publication_types: ["1"]
abstract: ""
featured: false
publication: "*2017 International Mixed Signals Testing Workshop (IMSTW)*"
---

