---
title: "Self-Learning RF Receiver Systems: Process-Aware Real-Time Adaptation to Channel Conditions for Low Power Operation"
date: 2016-01-01
publishDate: 2020-03-20T13:17:38.013226Z
authors: ["Debashis Banerjee", "Barry Muldrey", "Xian Wang", "Shreyas Sen", "Abhijit Chatterjee"]
publication_types: ["2"]
abstract: "Prior research has established that dynamically trading-off the performance of the radio-frequency (RF) front-end for reduced power consumption across changing channel conditions, using a feedback control system that modulates circuit and algorithmic level “tuning knobs” in real-time based on received signal quality, leads to significant power savings. It is also known that the optimal power control strategy depends on the process conditions corresponding to the RF devices concerned. This leads to an explosion in the search space needed to find the best feedback control strategy, across all combinations of channel conditions and receiver process corners, making simulation driven optimal control law design impractical and computationally infeasible. Since this problem is largely intractable due to the above complexity of simulation, we propose a self-learning strategy for adaptive RF systems. In this approach, RF devices learn their own performance vs. power consumption vs. tuning knob relationships “on-the-fly” and formulate the most power-optimal control strategy for real-time adaptation of the RF system using neural-network based learning techniques during real-time operation. The methodology is demonstrated using SISO & MIMO RF receiver front-ends as test vehicles and is supported by hardware validation leading to 2.5X-3X power savings with minimal overhead."
featured: false
publication: ""
---

