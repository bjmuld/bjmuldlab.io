---
title: "Low Cost Sparse Multiband Signal Characterization Using Asynchronous Multi-Rate Sampling: Algorithms and Hardware"
date: 2015-01-01
publishDate: 2020-03-20T13:17:38.012943Z
authors: ["Nicholas Tzou", "Debesh Bhatta", "Barry J. Muldrey", "Thomas Moon", "Xian Wang", "Hyun Choi", "Abhijit Chatterjee"]
publication_types: ["2"]
abstract: "Characterizing the spectrum of sparse wideband signals of high-speed devices efficiently and precisely is critical in high-speed test instrumentation design. Recently proposed sub-Nyquist rate sampling systems have the potential to significantly reduce the cost and complexity of sparse spectrum characterization; however, due to imperfections and variations in hardware design, numerous implementation and calibration issues have risen and need to be solved for robust and stable signal acquisition. In this paper, we propose a low-cost and low-complexity hardware architecture and associated asynchronous multi-rate sub-Nyquist rate sampling based algorithms for sparse spectrum characterization. The proposed scheme can be implemented with a single ADC or with multiple ADCs as in multi-channel or band-interleaved sensing architectures. Compared to other sub-Nyquist rate sampling methods, the proposed hardware scheme can achieve wideband sparse spectrum characterization with minimum cost and calibration effort. A hardware prototype built using off-the-shelf components is used to demonstrate the feasibility of the proposed approach."
featured: false
publication: ""
tags: ["Asynchronous multi-rate sampling", "Low cost spectrum sensing and characterization", "Sub-Nyquist rate sampling"]
doi: "10.1007/s10836-015-5505-9"
---

