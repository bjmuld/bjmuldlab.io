---
title: "Post Silicon Validation of Analog/Mixed Signal/RF Circuits and Systems: Recent Advances"
date: 2016-07-01
publishDate: 2020-03-20T10:54:18.201732Z
authors: ["A. Chatterjee", "S. Deyati", "B. J. Muldrey"]
publication_types: ["1"]
abstract: "Technology scaling along with unprecedented levels of device integration has led to increasing numbers of analog/mixed-signal/RF design bugs escaping into silicon. Such bugs are manifested under specific system-on-chip (SoC) operating conditions and their effects are difficult to predict a-priori. This paper describes recent advances in detecting and diagnosing such bugs using \"guided\" stochastic test stimulus generation algorithms. A key challenge is that unlike traditional test generation for manufacturing test that is predicated on known failure mechanisms, the nature of design bugs is generally unknown and must be discovered on-the-fly. Classes of design errors from undesired capacitive coupling and incorrect biasing conditions to incorrect guard-banding of designs are considered. It is shown that high design bug coverage can be obtained over a range of test cases."
featured: false
publication: "*2016 IEEE 21st International Mixed-Signal Testing Workshop (IMSTW)*"
tags: ["Integrated circuit modeling", "analog circuits", "analogue integrated circuits", "Radio frequency", "system-on-chip", "Hardware", "mixed analogue-digital integrated circuits", "Computer bugs", "mixed signal circuits", "elemental semiconductors", "Si", "silicon", "failure analysis", "radiofrequency integrated circuits", "technology scaling", "SoC", "Voltage measurement", "Delays", "bug detection", "bug diagnosis", "capacitive coupling", "failure mechanisms", "guided stochastic test stimulus generation", "incorrect biasing conditions", "incorrect guard-banding", "radiofrequency circuits", "silicon validation"]
doi: "10.1109/IMS3TW.2016.7524222"
---

