---
title: 'Design Confidence, Complexity, and Scaling: The Present and Future of System Validation'
summary: Invited talk at The University of Mississippi.
event: Invited talk at the University of Mississippi
location: 'Oxford, MS'
date: 2020-03-21T20:14:15.627Z
date_end: 2020-02-25T05:00:00.000Z
all_day: false
publishDate: ''
featured: false
authors:
  - barry
tags:
  - validation
  - testing
  - complexity
image:
  filename: featured
  focal_point: Smart
  preview_only: false
abstract: "Traditional SoC design-flows are under existential threat in the Post-Moore era: extracted VLSI standard-cell libraries
can take months to simulate at advanced process nodes; however, validation of primitive cells is not equivalent to the validation
of an exascale computing system or a 20 gigadevice chip. High-order component-level behaviors including various modes
of coupling and physical phenomena like hot-carrier injection threaten to play increasing roles in system dynamics,
and are inherently difficult to simulate. How do we build confidence without chip-scale physical simulation?
This talk analyzes the fundamental assumptions at the heart of the hierarchy- and abstraction-based design paradigm,
proposes new methodologies for validating and testing designs both pre- and post-silicon, and interrogates the possibility
of validating an infinite-transistor SoC."
---
