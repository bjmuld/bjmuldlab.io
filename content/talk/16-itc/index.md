---
title: 'DE-LOC: Debugging Under Limited Observation and Control'
summary: 'A presentation at ITC ''16 in Fort Worth, TX.'
abstract: >-
  In the modern mixed-signal SoC design cycle, designers are frequently tasked
  with detecting and diagnosing behavioral discrepancies between design
  descriptions given at different levels of hierarchy, e.g. behavioral vs.
  transistor level descriptions or behavioral/transistor level descriptions vs.
  fabricated silicon. One problem is detection, to determine if behavioral
  differences between design descriptions exist. If such differences (anomalies)
  are detected, then diagnosis is concerned with identifying the module in a
  hierarchical design description of the system that is most likely the root
  cause of the anomaly (typically under the constraint that only the primary
  outputs of the top-level hierarchies are observed. Previously proposed
  machine-learning classifiers require prior knowledge about the kinds of likely
  design errors typically encountered. In this work, we present a novel
  technique for the algorithmic foundation of circuit diagnosis predictions
  which does not require any assumptions about the nature of design errors. Our
  method employs iterative and alternate on-the-fly test generation and
  least-squares fitting of embedded low-order nonlinear filters to produce a
  best-guess estimate of the root cause of the anomaly. Experiments are
  conducted on two test vehicles, an RF transceiver and a phase-locked loop,
  several bug models are implemented, and the system's diagnosis predictions are
  analyzed.
event: International Test Conference
location: 'Fort Worth, TX'
date: 2016-11-16T14:26:17.393Z
date_end: ''
all_day: true
publishDate: ''
featured: true
image:
  filename: system_overview_chatupdate.svg
  focal_point: Smart
  preview_only: false
---
