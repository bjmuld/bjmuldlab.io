---
title: 'Designing Embedded Electronics, the Sanity-Preserving Way'
event: 'Invited talk, School of Industrial Design'
location: Savannah College of Art and Design
date: 2020-01-15T13:37:52.406Z
date_end: ''
all_day: false
publishDate: ''
featured: true
authors:
  - barry
image:
  filename: 0j3125.600.jpg
  focal_point: Smart
  preview_only: false
---
