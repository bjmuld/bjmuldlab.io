---
title: >-
  Diversity vs. Complexity: A Discussion of Dr. Mirta Galesic's "Social Learning
  Strategies..."
date: 2020-03-21T12:28:04.304Z
subtitle: ' '
summary: >-
  A brief discussion of Dr. Galesic's analysis of diversity's role in tackling
  complex problems.
draft: false
featured: true
authors:
  - barry
tags:
  - social-theory
  - diversity
  - complexity
  - choice
  - decision
  - value-alignment
categories:
  - social
image:
  filename: diversity_pic_anon_730d46_f11313.jpeg
  focal_point: Smart
  preview_only: false
---
Nearly every aspect of human life including food production, war-making, and community building is dependent at its core on the exchange of information and outcomes of group decision-making. On a bit larger scale, evolutionary trajectories are themselves the products of information exchange and group activity. Scientists have long investigated the differences between the capabilities of groups and those of individuals [1]: the field of game theory explains the emergence of cooperation as an optimal strategy [2]; work in networked controls theory explains the emergence of remarkable and unexpected ensemble properties of swarms [3]; and insights to biology have led to an understanding of information transmission across generations and through species [4]. A recent piece of work from Dr. Mirta Galesic at the Santa Fe Institute draws together components from these fields and others and considers what aspects of groups and their dynamics enable and promote the solution of high-complexity problems [5].

Dr. Galesic and her colleagues examine groups making decisions on an on-
going basis for solving mixtures of problems. They distinguish between problems in which individuals are relatively capable of a solution on their own and high-complexity problems, in which all individuals are generally incapable of providing a solution on their own. The researchers investigate group size, intra-group communication, information assimilation patterns in members, and the concepts of local and global information as they relate to decision-making outcomes in high- and low-complexity situations. Their findings illustrate that certain characteristics of the group reliably affect outcome and all have a direct
relationship to informational diversity. The underlying structures they identify can enable us to make informed predictions about the quality and efficiency of decision-making groups so that we can design better problem-solving groups and capacities to enable greater horizons for life on earth.

### The Dimensions of Group Diversity

The researchers analyze and breakdown the salient attributes of groups in the following ways; their research reveals the mechanisms by which these characteristics influence groups’ performance and how they relate to the informational diversity inside the group:

##### Group Size

Perhaps its most evident trait, a group’s size primarily affects the degree to which the group is an averaged version of its individual members and their traits. In smaller groups, there is less “averaging” and therefore more informational diversity. Large groups excel on problems which generalize; their averaging effects lead to good outcomes; small groups excel in complex environments, the informational diversity being more focused, concentrated among fewer individuals. [*]

##### Group Communications

Group connectivity encapsulates how many communication channels exist among members. It can be thought of as a measure of the cliquishness of the group. The more communication occurs across the group, the faster ideas can spread and assimilate, and the faster they will converge... for better and for worse. Convergence upon a solution is achieved through systematic reductions in information diversity. Rapid convergence on a suboptimal choice should be avoided!

##### Individuals’ Information Assimilation Strategies

In another dynamic of diversity, individual team members process and provide information through different mechanisms; for example some individuals are highly opinionated, and their contributions will tend to include fewer strains of their collaborators’ ideas, whereas others might identify a leader within the group whose contributions they will reinforce and reflect. Headstrong members will tend to slow down consensus and preserve diversity, whereas deferent members will assimilate quickly and reduce group diversity. In high-complexity situations, the groups which were able to maintain diversity for the longest duration were capable of reaching the greater outcomes. The price paid, however, is in the currency of time.

##### Local v. Global Information

In the face of complex problems, it may be
unreasonable to expect individuals to be capable of navigating the environment as a whole. This is analogous to domain expertise; an airline pilot’s knowledge of air traffic patterns does not give him additional insights into how regional governments could structure the financing of airports. On a complex landscape, organizational structures which require individuals to comprehend the entirety of the problem tend to systematically fall short relative to those which only require for regional expertise.

[*]: It seems this postulation assumes that the initial quantity of informational diversity scales sublinearly with group size; that the individuals are drawn from a distribution which exhibits a tendency toward the central limit; something that complex and high-dimensional variables
(like people) don’t necessarily exhibit.

### Assembling Optimal Groups

While variation in identity is often a good indication of informational diversity, it is not absolute. Variation in knowledge, experience, perspectives, and expertise among group members all contribute to the informational diversity of the group.
Variation in haircuts does not. Groups are formed in hopes that they can achieve outcomes greater than what its individual members could reach by themselves. This beneficial aspect to grouping is a reflection of the phenomenon known as “superlinearity.” A feature of superlinearity manifests as capability in excess of the sum of the parts.

In the context of low complexity, the linearity of the large group can be advantageous. Its averaging effect is desirable because it stabilizes outcomes over multiple problem solving sessions. Large groups can also be advantageous when used to cancel out the random error present among equally (in)accurate individuals, revealing solutions closer to optimal [6]. In the context of high-complexity, however, the averaging effects of large groups tends to ensure a solution worse than the best individual might otherwise provide. Smaller groups’ nonlinearity enables dynamics which identify the few bits of information which uniquely possess high-value and give them heavy emphasis.

Dr. Galesic’s work suggests that if the complexity of problems confronted by a group is known, we can estimate an optimal group size which will maximize outcomes over the lifetime of the group. And so, we can turn to the record of past performance for an indication of the complexity of problems faced by a group, and either change the group’s size or deploy tactics to otherwise modulate the rate of information convergence. Finding that it has been under-performing in an adversely complex environment, a group can implement strategies to encourage diversity to linger: reducing the size of the group, reducing meeting frequency, operational rules which slow down consensus building, and the creation of focused, specialty sub-teams all can slow down the reduction of diversity.
Through their work, Dr. Galesic and her team have identified important mechanisms by which solutions evolve through groups of decision-makers, and they’ve provided a practicable analytical framework for understanding and influencing outcomes.

### References

[1]: J. A. Nicolas et al., Essai sur l’application de l’analyse à la probabilité des
décisions rendues à la pluralité des voix. Par M. le marquis de Condorcet....
de l’Imprimerie Royale, 1785.

[2]: D. M. Kreps, P. Milgrom, J. Roberts, and R. Wilson, “Rational cooperation
in the finitely repeated prisoners’ dilemma,” Journal of Economic theory,
vol. 27, no. 2, pp. 245–252, 1982.

[3]: C. W. Reynolds, “Flocks, herds and schools: A distributed behavioral model,” in ACM SIGGRAPH computer graphics, ACM, vol. 21, 1987, pp. 25–34.

[4]: D. C. Krakauer, “Darwinian demons, evolutionary complexity, and information maximization,” Chaos: An Interdisciplinary Journal of Nonlinear Science, vol. 21, no. 3, p. 037 110, 2011.

[5]: D. Barkoczi and M. Galesic, “Social learning strategies modify the effect of network structure on group performance,” Nature communications, vol. 7, p. 13 109, 2016.

[6]: F. Galton, “Vox populi (the wisdom of crowds),” Nature, vol. 75, no.7, pp. 450–451, 1907.
