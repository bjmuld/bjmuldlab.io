#!/usr/bin/env bash

for method in xpdf tika pdftotext pyPdf pdfminer
do
    for trial in {1..5}
    do
        t=$(./update_website.py -y -P -p "${method}" | grep 'elapsed time:' | cut -d ':' -f 2)
        echo "${method}_${trial}: ${t}"
    done
done
